const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});

//--------------------------------------------------------------------
const CMToken = 'a57d650d9f7b8e4e6ddafd00a34b9a3d621afe9d';  // the token is used to access GitHub API
const githubOwner = 'Marathon-fan';
const repoName = 'testCM';
import { GraphQLClient } from 'graphql-request' 
const graphQLClient = new GraphQLClient('https://api.github.com/graphql', {
  headers: {
    Authorization: `Bearer ${CMToken}`,
  }
})
let github = require('octonode');
var githubClient = github.client(CMToken);

//----------------------------------

const query1b = `{
  repository(owner: "${githubOwner}", name: "${repoName}") {
    pullRequests(last: 5, states:CLOSED) {
      nodes {
        number
        state 
        mergeable         
        closedAt
      }
    }
  }
}  
`

app.get("/api/listofclosedpullrequests", function (req, res, next) {
  console.log('listofclosedpullrequests is called in node side', query1b);  
  graphQLClient.request(query1b).then(
    data => {
        console.log('query1b', JSON.stringify(data, null, '  '));
        res.status(200).json({
          message: "a list of closed pull requests fetched successfully!",
          pullrequests: data.repository.pullRequests.nodes
        });
      }
    );
});

//----------------------------------

const query2 = `{
  repository(owner: "${githubOwner}", name: "${repoName}") {
    pullRequests(last: 5, states:OPEN) {
      nodes {
        number
        state 
        mergeable         
        closedAt
      }
    }
  }
}
`
app.get("/api/listofopenpullrequests", function (req, res, next) {
  console.log('listofopenpullrequests is called in node side', query2);  
  graphQLClient.request(query2).then(
    data => {
        console.log('query2', JSON.stringify(data, null, '  '));
        res.status(200).json({
          message: "a list of open pull requests fetched successfully!",
          pullrequests: data.repository.pullRequests.nodes
        });
      }
    );
});


//----------------------------------

app.put("/api/mergeopenpullrequest", function (req, res, next) {
  try {
    console.log('mergeopenpullrequest is called in node side');  
    console.log('req.body.number', req.body.number);
    const mergeOpenPullRequestURL = `https://api.github.com/repos/${githubOwner}/${repoName}/pulls/${req.body.number}/merge`;   // 
    githubClient.put(mergeOpenPullRequestURL, {}, function (err, status, body, headers) {
      console.log(body); //json object
      res.status(200).json({
        message: body.message
      });
    });
  } catch (ex) {
    console.log('will not execute');
  }
});

module.exports = app;

