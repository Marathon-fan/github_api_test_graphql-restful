import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosedPullRequestComponent } from './closed-pull-request.component';

describe('ClosedPullRequestComponent', () => {
  let component: ClosedPullRequestComponent;
  let fixture: ComponentFixture<ClosedPullRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosedPullRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosedPullRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
