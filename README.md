
# readme   

## how to run it? 

```sh
# install 
npm install

# start back end 
forever start start.js 

# start front end
ng serve

# then navigate to http://localhost:4200/
click "See a list of closed pull requests for a given repo(Marathon-fan/testCM)" button to see the lastest 5 closed pull requests from repo repo(Marathon-fan/testCM)

click "See a list of open pull requests for a given repo(Marathon-fan/testCM)" button to see the lastest 5 open pull requests for a given repo(Marathon-fan/testCM

input the pull request number, and click "Merge open pull requests for a given repo(Marathon-fan/testCM)" button to merge the pull request

```

## Angular classes, components, etc

```sh
ng g c github/closed-pull-request
ng g c github/open-pull-request
ng g c github/merge-open-pull-request
ng g class github/shared/pull-request

```


## from develop branch checkout a new branch, write code to it, push it and swich back to develop branch  

```
git checkout -b feature7 && echo "feature7" > feature7.txt && git add -A && git commit -m "add feature7.txt" && git push --set-upstream origin feature7 && git checkout develop 
```
