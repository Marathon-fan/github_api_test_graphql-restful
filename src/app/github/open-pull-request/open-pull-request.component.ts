import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { PullRequest } from '../shared/pull-request';

@Component({
  selector: 'app-open-pull-request',
  templateUrl: './open-pull-request.component.html',
  styleUrls: ['./open-pull-request.component.css']
})
export class OpenPullRequestComponent implements OnInit {

  public prArray: PullRequest[] = null;
  columns: string[] = ["number", "state", "mergeable"];

  constructor(private http: HttpClient) {}

  ngOnInit() {
  }

  onGetOpenPullRequest() {
    console.log("function called");
    this.http
    .get<any>(
      "http://localhost:3000/api/listofopenpullrequests"
    )
    .subscribe( res => {
      console.log(res);
      this.prArray = res.pullrequests;

    });
  }

}
