import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-merge-open-pull-request',
  templateUrl: './merge-open-pull-request.component.html',
  styleUrls: ['./merge-open-pull-request.component.css']
})
export class MergeOpenPullRequestComponent implements OnInit {

  constructor(private http: HttpClient) {}
  mergeResult: String = null;
  number: String = "1";
  ngOnInit() {
  }

  onMergeClosedPullRequest() {
    console.log("onMergeClosedPullRequest function called", );
    this.http
    .put(
      "http://localhost:3000/api/mergeopenpullrequest", {number: this.number}
    )
    .subscribe( res => {
      console.log(res);
      this.mergeResult = JSON.stringify(res);
    });
  }

}





