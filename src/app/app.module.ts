import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatExpansionModule
} from "@angular/material";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { ClosedPullRequestComponent } from './github/closed-pull-request/closed-pull-request.component';
import { OpenPullRequestComponent } from './github/open-pull-request/open-pull-request.component';
import { MergeOpenPullRequestComponent } from './github/merge-open-pull-request/merge-open-pull-request.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ClosedPullRequestComponent,
    OpenPullRequestComponent,
    MergeOpenPullRequestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
