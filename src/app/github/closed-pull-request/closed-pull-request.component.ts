import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { PullRequest } from '../shared/pull-request';

@Component({
  selector: 'app-closed-pull-request',
  templateUrl: './closed-pull-request.component.html',
  styleUrls: ['./closed-pull-request.component.css']
})
export class ClosedPullRequestComponent implements OnInit {

  public prArray: PullRequest[] = null;
  columns: string[] = ["number", "state", "mergeable", "closedAt"];

  constructor(private http: HttpClient) {}

  ngOnInit() {
  }

  onGetClosedPullRequest() {
    console.log("function called");
    this.http
    .get<any>(
      "http://localhost:3000/api/listofclosedpullrequests")
    .subscribe( res => {
      console.log(res);
      this.prArray = res.pullrequests;

    });
  }
}

