import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeOpenPullRequestComponent } from './merge-open-pull-request.component';

describe('MergeOpenPullRequestComponent', () => {
  let component: MergeOpenPullRequestComponent;
  let fixture: ComponentFixture<MergeOpenPullRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeOpenPullRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeOpenPullRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
