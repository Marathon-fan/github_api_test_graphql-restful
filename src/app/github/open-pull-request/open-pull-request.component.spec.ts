import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenPullRequestComponent } from './open-pull-request.component';

describe('OpenPullRequestComponent', () => {
  let component: OpenPullRequestComponent;
  let fixture: ComponentFixture<OpenPullRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenPullRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenPullRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
