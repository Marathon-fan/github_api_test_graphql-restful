export class PullRequest {
    number: string;
    state: string;
    mergeable: string;
    closedAt: number;
}

